﻿using System;

namespace ScoreBoard.Models
{
    public class Team
    {
        public string Name { get; set; }

        public Team(string name)
        {
            Name = name;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !this.GetType().Equals(obj.GetType()))
                return false;

            Team team = (Team)obj;

            return this.Name.Equals(team.Name, StringComparison.OrdinalIgnoreCase);
        }

        public override int GetHashCode()
        {
            return this.Name.GetHashCode();
        }
    }
}
