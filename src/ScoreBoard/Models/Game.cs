﻿using System;
using System.Collections.Generic;

namespace ScoreBoard.Models
{
    public class Game
    {
        public Team Home { get; set; }

        public Team Away { get; set; }

        public KeyValuePair<int, int> Score { get; set; }

        public DateTime CreatedAt { get; }

        public bool Removed { get; set; }

        public int TotalScore
        {
            get
            {
                return this.Score.Key + this.Score.Value;
            }
        }

        public Game(Team home, Team away)
        {
            Home = home;
            Away = away;
            Score = new KeyValuePair<int, int>(0, 0);
            CreatedAt = DateTime.UtcNow;
        }

        public override string ToString()
        {
            return $"{Home.Name} - {Away.Name}: {Score.Key} - {Score.Value}";
        }
    }
}
