﻿using ScoreBoard.Models;
using System;
using System.Collections.Generic;

namespace ScoreBoard
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Football World Cup is going to start!");

            ScoreBoard scoreBoard = new();

            Team spain = new("Spain");
            Team france = new("France");
            Team italy = new("Italy");
            Team unitedKingdom = new("United Kingdom");
            Team brazil = new("Brazil");
            Team unitedStates = new("United States");

            Console.WriteLine("First game of the day is Spain vs France!");
            scoreBoard.StartGame(spain, france);
            Console.WriteLine(scoreBoard.ToString());
            Console.WriteLine("Gooooooaaaal, goal, goal of Spain!");
            scoreBoard.UpdateScore(spain, france, new KeyValuePair<int, int>(1, 0));
            Console.WriteLine(scoreBoard.ToString());
            Console.WriteLine("Other games of the day are going to start!");
            scoreBoard.StartGame(italy, unitedKingdom);
            scoreBoard.StartGame(brazil, unitedStates);
            Console.WriteLine(scoreBoard.ToString());
            Console.WriteLine("Gooooooaaaal, goal, goal of United Kingdom!");
            scoreBoard.UpdateScore(italy, unitedKingdom, new KeyValuePair<int, int>(0, 1));
            Console.WriteLine(scoreBoard.ToString());
            Console.WriteLine("France ties!");
            scoreBoard.UpdateScore(spain, france, new KeyValuePair<int, int>(1, 1));
            Console.WriteLine(scoreBoard.ToString());
            Console.WriteLine("Another one of United Kingdom!");
            scoreBoard.UpdateScore(italy, unitedKingdom, new KeyValuePair<int, int>(0, 2));
            Console.WriteLine(scoreBoard.ToString());
            Console.WriteLine("Gooooooaaaal, goal, goal of Spain! 2 - 1");
            scoreBoard.UpdateScore(spain, france, new KeyValuePair<int, int>(2, 1));
            Console.WriteLine(scoreBoard.ToString());
            Console.WriteLine("Spain wins first match of World Cup by 2 - 1");
            Console.WriteLine(scoreBoard.ToString());
            scoreBoard.FinishGame(spain, france);
            Console.WriteLine("Italy loses against United Kingdom by 0 - 2");
            Console.WriteLine(scoreBoard.ToString());
            scoreBoard.FinishGame(italy, unitedKingdom);
            Console.WriteLine("Brazil can do anything against United states defense. It's a tie!");
            Console.WriteLine(scoreBoard.ToString());
            scoreBoard.FinishGame(brazil, unitedStates);

            Console.WriteLine(scoreBoard.ToString());
        }
    }
}
