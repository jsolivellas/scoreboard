﻿using ScoreBoard.Models;
using System.Collections.Generic;
using System.Linq;

namespace ScoreBoard
{
    public class ScoreBoard
    {
        private readonly List<Game> _games;

        /// <summary>
        /// Get a list of games by total score. Those games with the same total score will be returned ordered by the most recently added to our system.
        /// </summary>
        public IList<Game> Summary
        {
            get
            {
                return _games.Where(g => !g.Removed).OrderByDescending(g => g.TotalScore).ThenByDescending(g => g.CreatedAt).ToList();
            }
        }

        public ScoreBoard()
        {
            _games = new List<Game>();
        }

        /// <summary>
        /// Start a game with initial score of 0 - 0.
        /// </summary>
        /// <param name="home">Home team.</param>
        /// <param name="away">Away team.</param>
        /// <returns>Game if Game is successfully started; otherwise, null.</returns>
        public Game StartGame(Team home, Team away)
        {
            if (home == null || away == null)
                return null;

            if (home.Equals(away))
                return null;

            Game game = this.GetGameByTeams(home, away);

            if (game != null)
                return game;

            game = new(home, away);
            _games.Add(game);

            return game;
        }

        /// <summary>
        /// Remove the first not finished game from the scoreboard.
        /// </summary>
        /// <param name="home">Home team.</param>
        /// <param name="away">Away team.</param>
        /// <returns>true if game is successfully removed; otherwise, false. This method also returns false if game was not found in the scoreboard.</returns>
        public bool FinishGame(Team home, Team away)
        {
            if (home == null || away == null)
                return false;

            Game game = this.GetGameByTeams(home, away);

            if (game == null)
                return false;

            game.Removed = true;

            return true;
        }

        /// <summary>
        /// Sets game score.
        /// </summary>
        /// <param name="home">Home team.</param>
        /// <param name="away">Away team.</param>
        /// <param name="score">Game score.</param>
        /// <returns>Game with setted score if Game is successfully updated; otherwise, Game with current score. This method also returns null if game was not found in the scoreboard.</returns>
        public Game UpdateScore(Team home, Team away, KeyValuePair<int, int> score)
        {
            Game game = this.GetGameByTeams(home, away);

            if (game == null)
                return null;

            if (score.Key < 0 || score.Value < 0 ||
                score.Key < game.Score.Key || score.Value < game.Score.Value)
                return game;

            game.Score = score;

            return game;
        }

        public override string ToString()
        {
            return string.Join('\n', this.Summary);
        }

        /// <summary>
        /// Get game with specified home and away teams.
        /// </summary>
        /// <param name="home">Home team.</param>
        /// <param name="away">Away team.</param>
        /// <returns>Game if exists; otherwise, null.</returns>
        private Game GetGameByTeams(Team home, Team away)
        {
            return _games.FirstOrDefault(g => g.Home.Equals(home) && g.Away.Equals(away) && !g.Removed);
        }
    }
}
