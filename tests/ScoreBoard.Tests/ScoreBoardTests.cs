using ScoreBoard.Models;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace ScoreBoard.Tests
{
    public class ScoreBoardTests
    {
        [Fact]
        public void StartGameTest()
        {
            ScoreBoard scoreBoard = new();

            Team teamA = new("A");
            Team teamB = new("B");

            Game game = scoreBoard.StartGame(teamA, teamB);

            Assert.NotNull(game);
            Assert.True(game.Score.Key == 0 && game.Score.Value == 0);
        }

        [Fact]
        public void StartGameNullReferenceTest()
        {
            ScoreBoard scoreBoard = new();

            Game game = scoreBoard.StartGame(null, null);

            Assert.Null(game);
        }

        [Fact]
        public void StartGameSameTeamTest()
        {
            ScoreBoard scoreBoard = new();

            Team teamA = new("A");

            Game game = scoreBoard.StartGame(teamA, teamA);

            Assert.Null(game);
        }

        [Fact]
        public void GameExistsTest()
        {
            ScoreBoard scoreBoard = new();

            Team teamA = new("A");
            Team teamB = new("B");

            Game game1 = scoreBoard.StartGame(teamA, teamB);
            Game game2 = scoreBoard.StartGame(teamA, teamB);

            Assert.Equal(game1, game2);
        }

        [Fact]
        public void RemovedGameExistsTest()
        {
            ScoreBoard scoreBoard = new();

            Team teamA = new("A");
            Team teamB = new("B");

            Game game1 = scoreBoard.StartGame(teamA, teamB);
            scoreBoard.FinishGame(teamA, teamB);
            Game game2 = scoreBoard.StartGame(teamA, teamB);

            Assert.NotEqual(game1, game2);
            Assert.True(!game2.Removed);
        }

        [Fact]
        public void FinishGameTest()
        {
            ScoreBoard scoreBoard = new();

            Team teamA = new("A");
            Team teamB = new("B");

            scoreBoard.StartGame(teamA, teamB);

            bool result = scoreBoard.FinishGame(teamA, teamB);

            Assert.True(result);
        }

        [Fact]
        public void FinishNonExistentGameTest()
        {
            ScoreBoard scoreBoard = new();

            Team teamA = new("A");
            Team teamB = new("B");

            bool result = scoreBoard.FinishGame(teamA, teamB);

            Assert.False(result);
        }

        [Fact]
        public void FinishNullReferenceTest()
        {
            ScoreBoard scoreBoard = new();

            bool result = scoreBoard.FinishGame(null, null);

            Assert.False(result);
        }

        [Fact]
        public void UpdateScoreTest()
        {
            ScoreBoard scoreBoard = new();

            Team teamA = new("A");
            Team teamB = new("B");

            scoreBoard.StartGame(teamA, teamB);

            Game game = scoreBoard.UpdateScore(teamA, teamB, new KeyValuePair<int, int>(1, 0));

            Assert.True(game.Score.Key == 1 && game.Score.Value == 0);
        }

        [Fact]
        public void UpdateScoreRemovedGameTest()
        {
            ScoreBoard scoreBoard = new();

            Team teamA = new("A");
            Team teamB = new("B");

            scoreBoard.StartGame(teamA, teamB);
            scoreBoard.FinishGame(teamA, teamB);

            Game game = scoreBoard.UpdateScore(teamA, teamB, new KeyValuePair<int, int>(1, 0));

            Assert.Null(game);
        }

        [Fact]
        public void UpdateScoreOfNonExistentGameTest()
        {
            ScoreBoard scoreBoard = new();

            Team teamA = new("A");
            Team teamB = new("B");

            Game game = scoreBoard.UpdateScore(teamA, teamB, new KeyValuePair<int, int>(1, 0));

            Assert.Null(game);
        }

        [Fact]
        public void UpdateWithNegativeScoreTest()
        {
            ScoreBoard scoreBoard = new();

            Team teamA = new("A");
            Team teamB = new("B");

            scoreBoard.StartGame(teamA, teamB);

            Game game = scoreBoard.UpdateScore(teamA, teamB, new KeyValuePair<int, int>(-1, -1));

            Assert.True(game.Score.Key == 0 && game.Score.Value == 0);
        }

        [Fact]
        public void UpdateWithLowerScoreTest()
        {
            ScoreBoard scoreBoard = new();

            Team teamA = new("A");
            Team teamB = new("B");

            scoreBoard.StartGame(teamA, teamB);

            Game game = scoreBoard.UpdateScore(teamA, teamB, new KeyValuePair<int, int>(1, 1));
            game = scoreBoard.UpdateScore(teamA, teamB, new KeyValuePair<int, int>(0, 0));

            Assert.True(game.Score.Key == 1 && game.Score.Value == 1);
        }

        [Fact]
        public void SummaryTotalScoreOrderTest()
        {
            ScoreBoard scoreBoard = new();

            Team teamA = new("A");
            Team teamB = new("B");

            scoreBoard.StartGame(teamA, teamB);
            scoreBoard.StartGame(teamB, teamA);

            scoreBoard.UpdateScore(teamA, teamB, new KeyValuePair<int, int>(5, 0));

            Assert.True(scoreBoard.Summary.FirstOrDefault().TotalScore == 5);
        }

        [Fact]
        public void SummaryIgnoreRemovedGamesTest()
        {
            ScoreBoard scoreBoard = new();

            Team teamA = new("A");
            Team teamB = new("B");

            scoreBoard.StartGame(teamA, teamB);
            scoreBoard.StartGame(teamB, teamA);

            scoreBoard.FinishGame(teamB, teamA);

            Assert.True(scoreBoard.Summary.Count == 1);
        }

        [Fact]
        public void SummaryTotalScoreAndCreationDateOrderTest()
        {
            ScoreBoard scoreBoard = new();

            Team teamA = new("A");
            Team teamB = new("B");
            Team teamC = new("C");

            scoreBoard.StartGame(teamA, teamB);
            scoreBoard.StartGame(teamB, teamA);
            scoreBoard.StartGame(teamC, teamA);

            scoreBoard.UpdateScore(teamA, teamB, new KeyValuePair<int, int>(5, 0));
            scoreBoard.UpdateScore(teamB, teamA, new KeyValuePair<int, int>(4, 2));
            scoreBoard.UpdateScore(teamC, teamA, new KeyValuePair<int, int>(3, 3));

            Assert.True(scoreBoard.Summary[0].Home == teamC);
            Assert.True(scoreBoard.Summary[1].Home == teamB);
            Assert.True(scoreBoard.Summary[2].Home == teamA);


        }
    }
}
